import React from 'react'
import {render} from 'react-dom'

import {UnControlled as CodeMirror} from 'react-codemirror2'
require('codemirror/mode/toml/toml');

import {default as Sidebar} from './Sidebar'


function App() {
  return(
    <>
      <CodeMirror
        options={{
          autofocus: true,
          mode: 'toml',
          theme: 'mdn-like',
          lineNumbers: true,
          scrollbarStyle: 'native',
          viewportMargin: Infinity
        }}
      />
      <Sidebar />
    </>
  )
}

render(<App />,  document.getElementById('app'));


