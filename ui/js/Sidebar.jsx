import React from 'react'

export default () => (
  <div className='sidebar'>
    <form>
      <div className="form-group">
        <label htmlFor="rabbitmqVersion">RabbitMQ Version</label>
        <select className="form-control">
          <option>3.7.14</option>
          <option>3.7.13</option>
        </select>
      </div>
    </form>
    <hr />
    <h4 className='text-muted'>Results</h4>
  </div>
)
