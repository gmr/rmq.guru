REVISION = $(shell git rev-parse HEAD | cut -b 1-7)

.PHONY: all
all: setup build

.PHONY: setup
setup: deps ui/node_modules

.PHONY: build
build: compile
	@ cd ui && npm run dev-sass
	@ cd ui && npm run dev-build

.PHONY: compile
compile:
	@ rebar3 compile

.PHONY: clean
clean:
	@ rm -rf static/site.css static/fonts/* static/js/*.js
	@ rm -rf _build

.PHONY: deps
deps:
	@ rebar3 get-deps

.PHONY: dist
dist:
	@ cd ui && npm run sass
	@ cd ui && npm run build
	@ rebar3 release

.PHONY: eslint
eslint:
	@ cd ui && npm run eslint

.PHONY: ui/node_modules
ui/node_modules:
	@ cd ui && npm install

.PHONY: run
run:
	rebar3 shell --apps rmq_guru
