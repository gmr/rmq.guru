%% ------------------------------------------------------------------
%% Cowboy Listener
%% ------------------------------------------------------------------
-module(rmq_guru_listener).

-behaviour(gen_server).

-export([start_link/0,
  init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-include("rmq_guru.hrl").

%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------

start_link() ->
  gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%% ------------------------------------------------------------------
%% gen_server Function Definitions
%% ------------------------------------------------------------------

init([]) ->
  Dispatch = rmq_guru_routes:get(),
  cowboy:start_clear(rmq_guru_cowboy_listener,
    [{port, port()}],
    #{env => #{dispatch => Dispatch}}),
  lager:info("Application started and listening"),
  {ok, {}}.

handle_call(_Request, _From, State) ->
  {noreply, State}.

handle_cast(_Request, State) ->
  {noreply, State}.

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  cowboy:stop_listener(rmq_guru_cowboy_listener).

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%% ------------------------------------------------------------------
%% Response handling behaviors
%% ------------------------------------------------------------------

on_response(Code, Headers, _Body, Req) when is_integer(Code), Code >= 400 ->
  lager:info("~p ~s ~s", [Code, cowboy_req:method(Req), cowboy_req:path(Req)]),
  Subtype = get_subtype(Headers),
  {Headers1, Body} = get_error_response(Code, Subtype, Req),
  Req1 = cowboy_req:reply(Code, merge_headers(Headers, Headers1), Body, Req),
  Req1;

on_response(Code, _Headers, _Body, Req) ->
  lager:info("~p ~s ~s", [Code, cowboy_req:method(Req), cowboy_req:path(Req)]),
  Req.

%% ------------------------------------------------------------------
%% Internal methods
%% ------------------------------------------------------------------

get_subtype(Headers) ->
  case proplists:get_value(?CONTENT_TYPE, Headers, []) of
    []                 -> <<"html">>;
    [_, _, Subtype]    -> Subtype;
    [_, _, Subtype, _] -> Subtype
  end.

get_error_response(Code, Subtype, _Req) ->
  case Subtype of
    <<"json">> ->
      Body = jsx:encode([{?ERROR, proplists:get_value(Code, ?STATUS_CODES)}]),
      Headers = [{?CONTENT_TYPE, ?MIME_TYPE_JSON},
        {?CONTENT_LENGTH, rmq_guru_util:get_body_size(Body)}],
      {Headers, Body}
  end.

merge_headers(Headers1, Headers2) ->
  orddict:merge(fun(_, X, Y) -> X, Y end,
    orddict:from_list(Headers1),
    orddict:from_list(Headers2)).

port() ->
  rmq_guru_util:get_int_from_env("HTTP_PORT", http_port).

timeout() ->
  rmq_guru_util:get_int_from_env("HTTP_TIMEOUT", http_timeout).
