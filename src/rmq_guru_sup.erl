%% ------------------------------------------------------------------
%% Primary Supervisor for rmq.guru processes
%% ------------------------------------------------------------------
-module(rmq_guru_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
    {ok, {{one_for_one, 5, 10},
          [?CHILD(rmq_guru_cuttlefish, worker),
           ?CHILD(rmq_guru_listener, worker)]}}.
