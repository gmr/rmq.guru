%% --------------------------------------------------------------------
%% Abstracted Cowboy listener routes for ease of editing and management
%% --------------------------------------------------------------------
-module(rmq_guru_routes).

-export([get/0]).


%% @spec get() -> list
%% @doc Return the list of routes by host for the Cowboy listener
%% @end
%%
get() ->
  cowboy_router:compile([
    {'_',
      [
        {
          "/",
          cowboy_static,
          {file, "static/index.html", [{mimetypes, {<<"text">>, <<"html">>, []}}]}
        },
        {
          "/robots.txt",
          cowboy_static,
          {file, "static/robots.txt", [{mimetypes, {<<"text">>, <<"plain">>, []}}]}
        },
        {
          "/favicon.ico",
          cowboy_static,
          {file, "static/favicon.ico", [{mimetypes, {<<"image">>, <<"x-icon">>, []}}]}
        },
        {
          "/api/",
          cowboy_static,
          {file, "static/redoc.html", [{mimetypes, {<<"text">>, <<"html">>, []}}]}
        },
        {
          "/static/[...]",
          cowboy_static,
          {dir, [<<"static">>], [{mimetypes, cow_mimetypes, all},
            {dir_handler, directory_handler}]}
        }
      ]
    }
  ]).
