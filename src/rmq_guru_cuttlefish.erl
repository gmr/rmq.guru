%% ------------------------------------------------------------------
%% Cuttlefish Integration
%% ------------------------------------------------------------------
-module(rmq_guru_cuttlefish).

-behaviour(gen_server).

-export([
  start_link/0,
  init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-include("rmq_guru.hrl").

-record(state, {schemas}).

%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------

start_link() ->
  gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%% ------------------------------------------------------------------
%% gen_server Function Definitions
%% ------------------------------------------------------------------

init([]) ->
  lager:info("Application started and listening"),
  {ok, #state{schemas=[{V, load_cuttlefish_schema(V)} || V <- ["3.7.14", "3.7.13"]]}}.

handle_call({validate, Version, Conf}, _From, State) ->
  Schema = proplists:get_value(Version, State#state.schemas),
  Parsed = conf_parse:parse(Conf),
  case cuttlefish_generator:map(Schema, Parsed) of
    {error, _, {errorlist, Errors}} ->
      {reply, {error, Errors}, State};
    _ -> {reply, ok, State}
  end;

handle_call(_Request, _From, State) ->
  {reply, ok, State}.

handle_cast(_Request, State) ->
  {_, _, V} = proplists:get_value("3.7.14", State#state.schemas),
  lager:info("Validator ~p~n", [V]),
  {noreply, State}.

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_OldVsn, State, _Extra) ->
  {ok, State}.


load_cuttlefish_schema(Version) ->
  lager:info("Processing schema for ~p~n", [Version]),
  Path = string:join(["static", "schema", Version, "*.schema"], "/"),
  cuttlefish_schema:files(filelib:wildcard(Path)).
